#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      kingston
#
# Created:     20/04/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>

"""The Employee incharge creates a memo and sends it to the

    **** Manager ===> COO Approval ===> accountant
    **** Account selects the type of payment... Onselection, Button of supplier, customer refund, view payment opens on each states (use context in hotel restaurant folio is a guest)

    """
#-------------------------------------------------------------------------------
from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.exceptions import except_orm, ValidationError

from datetime import datetime, timedelta
import time

class MemoStarter(models.Model):
    _name = 'internal.memo.custom'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    @api.onchange('vendor_bill')
    def _get_source_amount(self):
        self.update({'amountfig':self.vendor_bill.amount_total})

    @api.multi
    def _send_direct_message(self):
        pass

    name = fields.Char('Memo Description')
    date = fields.Datetime(string='Date',default=fields.Date.context_today, required=True, copy=False)
    employee_id = fields.Many2one('hr.employee', string = 'Responsible')
    dept_ids = fields.Char(string ='Department', related='employee_id.department_id.name',readonly = True, store =True)#compute='_department_id',)#related='employee_id.department_id.name', readonly = True, store=True)#
    vendor_bill = fields.Many2one('account.invoice', 'Reference')
    origin_ref = fields.Char(string = 'Memo',related='vendor_bill.origin', store =True) #compute='_memo_id',
    attachment_number = fields.Integer(compute='_compute_attachment_number', string='No. Attachments')
    description = fields.Char('Note')
    project_id = fields.Many2one('account.analytic.account', 'Project')
    amountfig = fields.Float('Amount')#, compute='_get_source_amount')#, related="vendor_bill.amount_total", store=True)
    description_two=fields.Text('Reasons')
    file_upload = fields.Binary('File Upload')


    to_partner=fields.Many2many('res.users', string ="Direct Recipients")


    state = fields.Selection([('submit', 'Draft'),
                                ('headunit', 'HOU'),
                                ('manager', 'GM'),
                                ('ed', 'ED'),
                                ('coo', 'MD/CEO'),
                                ('account', 'Accounts'),
                                ('post', 'Posted'),
                                ('done', 'Paid'),
                                ('refused', 'Refused')
                              ], string='Status', index=True, readonly=True, track_visibility='onchange', copy=False, default='submit', required=True,
        help='Expense Report State')
    payment_mode = fields.Selection([("csr", "Customer Refund"), ("sp", "Supplier Payment"),("dla", "Direct Labour Payment")], string="Payment Type",required=True,)#, states={'done': [('readonly', True)], 'post': [('readonly', True)]}, string="Payment By")

# employee submit to HOU for confirm
    @api.multi
    def button_employee_headunit(self): # vis_hou
        self.state = 'headunit'
        body = "The memo has been submitted to Head of Unit for Approval on %s" % (datetime.strftime(datetime.today(), '%d-%m-%y'))
        #records = self._get_followers()
        #followers = records#[self.ids[4]]['message_follower_ids']
        #self.message_post(body=body, subtype='mt_comment', message_type='notification',partner_ids=followers)
        subject ="Memo Notification"

        ########
        partner_ids = self.env['res.users'].browse(self.to_partner).ids
 # search domain to filter specific partners
        self.message_post(subject=subject,body=body, subtype='mt_comment',message_type='notification',partner_ids=partner_ids)#[(4, [partner_ids])])
        ########
# coo / ed to account
# hou submit to Gm
    @api.multi
    def button_hou_gmanager(self): # vis_hou_GM
        self.state = 'manager'
        body = "The memo has been submitted to Manager for Approval on %s" % (datetime.strftime(datetime.today(), '%d-%m-%y'))
        records = self._get_followers()
        followers = records
        self.message_post(body=body, subtype='mt_comment', message_type='notification',partner_ids=followers)
# coo / ed to account

    """
    records = self._get_followers(cr, uid, ids, None, None, context=context)
    followers = records[ids[0]]['message_follower_ids']
    self.message_post(cr, uid, ids, body=body,
            subtype='mt_comment',
            partner_ids=followers,
            context=context)"""
# gm to submit to ED If greater than amount > 1million, to accounts if amount < 1MILLION
# GM manager submit to ed
    @api.multi
    def button_gm_ed(self):
        one_mill = float(1000000)
        if self.amountfig <= one_mill:
            self.state = 'ed'
            body = "The Memo has been submitted to ED for Payment on %s" % (datetime.strftime(datetime.today(), '%d-%m-%y'))
            records = self._get_followers()
            followers = records
            self.message_post(body=body, subtype='mt_comment',message_type='notification',partner_ids=followers)
        elif self.amountfig >= one_mill:
            self.state = 'coo'
            body = "The Memo has been submitted to CEO for Approval on %s" % (datetime.strftime(datetime.today(), '%d-%m-%y'))
            records = self._get_followers()
            followers = records
            self.message_post(body=body, subtype='mt_comment',message_type='notification',partner_ids=followers)
        else:
            raise ValidationError('No Amount on the Choosen invoice')


    @api.multi
    def button_ed_to_coo(self): # vis_submit
        self.state = 'account'
        body = "The Memo has been submitted to Accounts for Payment on %s" % (datetime.strftime(datetime.today(), '%d-%m-%y'))
        records = self._get_followers()
        followers = records
        self.message_post(body=body, subtype='mt_comment',message_type='notification',partner_ids=followers)

# coo / ed to account
    @api.multi
    def button_coo_ed_account(self):#vis_account
        self.state = 'account'
        body = "The Memo has been confirmed and posted to Accounts from %s on %s" % (self.env.user.name,datetime.strftime(datetime.today(), '%d-%m-%y'))
        records = self._get_followers()
        followers = records
        self.message_post(body=body, subtype='mt_comment',message_type='notification',partner_ids=followers)

    def message_posts(self):
        body= "REFUSAL NOTIFICATION;\n %s" %(self.description_two)
        records = self._get_followers()
        followers = records
        self.message_post(body=body, subtype='mt_comment',message_type='notification',partner_ids=followers)

    @api.multi
    def button_account_post(self):#vis_coo
        self.state = 'post'

# coo submit to account

    @api.multi
    def refused_manager(self): #vis_account,
        '''self.state = 'refused'
        body = "General Manager has rejected the Memo on %s" % (datetime.strftime(datetime.today(), '%d-%m-%y'))
        records = self._get_followers()
        followers = records
        self.message_post(body=body, subtype='mt_comment',message_type='notification',partner_ids=followers)'''
        return {
              'name': 'Reason for Refusal',
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'send.memo.message',
              'type': 'ir.actions.act_window',
              'target': 'new',
              'context': {
                  'default_memo_record': self.id,
                  'default_date': self.date
              },
        }
    @api.multi
    def refused_ed(self): #vis_account,
        '''self.state = 'refused'
        body = "ED has rejected the Memo on %s" % (datetime.strftime(datetime.today(), '%d-%m-%y'))
        records = self._get_followers()
        followers = records
        self.message_post(body=body, subtype='mt_comment',message_type='notification',partner_ids=followers)'''
        return {
              'name': 'Reason for Refusal',
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'send.memo.message',
              'type': 'ir.actions.act_window',
              'target': 'new',
              'context': {
                  'default_memo_record': self.id,
                  'default_date': self.date
              },
        }
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    @api.multi
    def refuse_account(self): #vis_account,
        '''self.state = 'refused'
        body = "Account has rejected the Memo on %s" % (datetime.strftime(datetime.today(), '%d-%m-%y'))
        records = self._get_followers()
        followers = records
        self.message_post(body=body, subtype='mt_comment',message_type='notification',partner_ids=followers)'''



        return {
              'name': 'Reason for Refusal',
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'send.memo.message',
              'type': 'ir.actions.act_window',
              'target': 'new',
              'context': {
                  'default_memo_record': self.id,
                  'default_date': self.date
              },
        }


    @api.multi
    def refuse_coo(self): #vis_account,
        '''self.state = 'refused'
        body = "COO has rejected the Memo on %s" % (datetime.strftime(datetime.today(), '%d-%m-%y'))
        records = self._get_followers()
        followers = records
        self.message_post(body=body, subtype='mt_comment',message_type='notification',partner_ids=followers)'''

        return {
              'name': 'Reason for Refusal',
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'send.memo.message',
              'type': 'ir.actions.act_window',
              'target': 'new',
              'context': {
                  'default_memo_record': self.id,
                  'default_date': self.date
              },
        }
    @api.multi
    def button_cancel(self):
        self.state = 'submit'


    @api.multi
    def _compute_attachment_number(self):
        attachment_data = self.env['ir.attachment'].read_group([('res_model', '=', 'internal.memo.custom'), ('res_id', 'in', self.ids)], ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count']) for data in attachment_data)
        for expense in self:
            expense.attachment_number = attachment.get(expense.id, 0)

    @api.multi
    def action_get_attachment_view(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id('base', 'action_attachment')
        res['domain'] = [('res_model', '=', 'internal.memo.custom'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'internal.memo.custom', 'default_res_id': self.id}
        return res

    @api.onchange('employee_id')
    def _department_id(self):
        for y in self:
            department = y.employee_id.department_id.name
            y.dept_ids = department

    @api.onchange('vendor_bill')
    def _memo_id(self):
        for y in self:
            origin = y.vendor_bill.origin
            y.origin_ref = origin


    '''@api.depends('num_days', 'request_amount', 'payment_mode')
    def _compute_amount_three(self):
        for x in self:
            if x.payment_mode == 'bymonth':
                if x.num_days > 12:
                    raise UserError(_("Month Cannot Exceed 12 calendar Months"))
                else:
                    cal_Month = x.num_days * x.request_amount
                    x.total_amount = cal_Month
            elif x.payment_mode == 'byday':
                cal_day = x.num_days * x.request_amount
                x.total_amount = cal_day'''
    '''@api.multi
    def button_pay_supplier(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id('allocation', 'view_payment_action')
        #res['domain'] = [('res_model', '=', 'internal.memo.custom'), ('res_id', 'in', self.ids)]
        #res['context'] = {'default_res_model': 'internal.memo.custom', 'default_res_id': self.id}
        return res'''
    @api.multi
    def button_pay_supplier(self):#vis_post

        #print "########### "+ str(self.vendor_bill)
        xxxxlo = self.env['account.invoice'].search([('id', '=', self.vendor_bill.id)])
        self.write({'state':'done'})
        if not xxxxlo:
            raise except_orm(_('Error'),
                                 _('There is no related bills for this Memo. \
                                 Kindly create a vendor bill and try again.'))
        resp = {
            'type': 'ir.actions.act_window',
            'name': _('Supplier Reference'),
            'res_model': 'account.invoice',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'current',
            'res_id': xxxxlo.id
        }
        return resp
        '''self.ensure_one()
        res = self.env['ir.actions.act_window.view'].for_xml_id('account', 'action_invoice__supplier_tree1_view2')
        res['domain'] = [('res_model', '=', 'internal.memo.custom'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'internal.memo.custom', 'default_res_id': self.id}
        return res'''

    @api.multi
    def button_refund_customer(self): #vis_post
        self.ensure_one()
        self.write({'state':'done'})
        xxxb = self.env['account.move'].search([('ref', '=', self.origin_ref)])
        if not xxxb:
            raise except_orm(_('Error'),_('There is no related journal bills for this Memo'))
        respx = {'type':'ir.actions.act_window',
                'name':_('Supplier Referene'),
                'res_model':'account.move',
                'view_type':'form',
                'view_mode':'form',
                'target':'current',
                'res_id':xxxb.id
                }
        #res = self.env['ir.actions.act_window'].for_xml_id('allocation', 'view_payment_action')
        return respx

    @api.multi
    def button_dla_pay(self): #vis_post
        self.ensure_one()
        self.write({'state':'done'})
        res = self.env['ir.actions.act_window'].for_xml_id('account', 'action_account_payments_payable')

        return res




class Send_Message(models.Model):
    _name="send.memo.message"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    #_inherit='res.users'
    reason = fields.Char('Reason')#<tree string="Memo Payments" colors="red:state == 'account';black:state == 'manager';green:state == 'coo';grey:state == 'refused';">

    date = fields.Datetime('Date')
    resp=fields.Many2one('res.users','Responsible')#, default=self.write_uid.id)
    memo_record = fields.Many2one('internal.memo.custom','Memo ID')

    def _change_state(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        all_records = self.env['internal.memo.custom'].browse(active_ids)
        state = all_records.write({'state':'refused'})
        return state
    def _send_message(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        all_records = self.env['internal.memo.custom'].browse(active_ids)
        #message = all_records.write({'state':'refused'})
        #return state

    @api.multi
    def post_refuse(self):
        get_state = self.env['internal.memo.custom'].search([('id','=', self.memo_record.id)])
        reasons = "%s Refused the Memo because of the following reason: \n %s." %(self.env.user.name,self.reason)
        get_state.write({'description_two':reasons})
        get_state.message_posts()
        self._change_state()
        return{'type': 'ir.actions.act_window_close'}










